<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="">
                <img src="uploads/logo.png" class="img-square" alt="User Image" width="195" height="100">
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
           
            <li <?php echo ($page=='dashboard' ) ? "class='treeview active'" : ""; ?> >
                <a href="dashboard.php">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>


            <li <?php echo ($page=='masterfiles' ) ? "class='treeview active'" : ""; ?> >
                <a href="masterfiles.php">
                    <i class="fa fa-list"></i>
                    <span>Masterfiles</span>
                </a>
            </li>

            <li <?php echo ($page=='addpds' ) ? "class='treeview active'" : ""; ?> >
                <a href="add_pds.php">
                    <i class="fa fa-user"></i>
                    <span>Add PDS</span>
                </a>
            </li>

            <li <?php echo ($page=='usermgmt' ) ? "class='treeview active'" : ""; ?> >
                <a href="usermgmt.php">
                    <i class="fa fa-gears"></i>
                    <span>User Management</span>
                </a>
            </li>

        </ul>
    </section>
</aside>