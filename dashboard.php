<?php
@session_start();
include "config/dbconnect.php";
include "config/pdoConfig.php";
include "config/dbconfig.php";

if(@$_SESSION['admin']){
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | LGU</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="assets/plugins/iCheck/all.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="assets/dist/css/skins/_all-skins.min.css">
        <!-- demo style -->
        <!-- Custom Fonts -->
        <link rel="stylesheet" type="text/css" href="assets/asset/font-awesome-4.6.3/css/font-awesome.min.css">
        <link rel="shortcut icon" href="img/favicon.png">
        <!-- FullCalendar -->
        <link href='calendar/css/fullcalendar.css' rel='stylesheet' />
        <!-- jQuery Version 1.11.1 -->
        <script src="calendar/js/jquery.js"></script>
        <!-- FullCalendar -->
        <script src='calendar/js/moment.min.js'></script>
        <script src='calendar/js/fullcalendar.min.js'></script>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <!-- sidebar-collapse -->
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="#" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>LGU</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Admin</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
                    <?php
                $id = $_SESSION['id'];

                $showSql = "SELECT * FROM tbl_user WHERE id = '$id'";
                $showResult = mysqli_query($con, $showSql);

                if ($showResult) {
                    while ($row = mysqli_fetch_array($showResult)) {
                        $fullname =  $row['fullname'];
                        $photo =  $row['photo'];
                        $position =  $row['position'];
                    }
                }
            ?>
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <!-- Messages: style can be found in dropdown.less-->
                                <!-- User Account: style can be found in dropdown.less -->
                                <li class="dropdown user user-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gears"></i></a>
                                    <ul class="dropdown-menu">
                                        <!-- User image -->
                                        <li class="user-header">
                                            <img src="uploads/<?php echo $photo; ?>" class="img-circle" alt="User Image">
                                            <p>
                                                <?php echo $fullname; ?>
                                                <small><?php echo $position; ?></small>
                                            </p>
                                        </li>
                                        <!-- Menu Footer-->
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                                            </div>
                                            <div class="pull-right">
                                                <a href="config/logout.php" class="btn btn-default btn-flat">Sign out</a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                </nav>
            </header>

            
            <!-- SIDEBAR NAVIGATION MENU AND LOGO -->
            <?php $page = 'dashboard'; include('navigation.php'); ?>
            <!-- END SIDEBAR -->


            <?php
            date_default_timezone_set('UTC');
            ?>
                <script>
                var d = new Date(<?php echo time() * 1000 ?>);

                function digitalClock() {
                    d.setTime(d.getTime() + 1000);
                    var hrs = d.getHours();
                    var mins = d.getMinutes();
                    var secs = d.getSeconds();
                    mins = (mins < 10 ? "0" : "") + mins;
                    secs = (secs < 10 ? "0" : "") + secs;
                    var apm = (hrs < 12) ? "am" : "pm";
                    hrs = (hrs > 12) ? hrs - 12 : hrs;
                    hrs = (hrs == 0) ? 12 : hrs;
                    var ctime = hrs + ":" + mins + ":" + secs + " " + apm;
                    document.getElementById("clock").firstChild.nodeValue = ctime;
                }
                window.onload = function() {
                    digitalClock();
                    setInterval('digitalClock()', 1000);
                }
                </script>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Dashboard
                            <small>Monitoring of Data</small>
                            <!-- Timer -->
                            <small class="col-sm-offset-3 text-success">Clock :</small>
                            <small class="text-success"><div id="clock"> </div></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </section>
                    <!-- Main content -->
                    <section class="content">
                        <!-- Small boxes (Stat box) -->
                        <div class="row">


                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->
                <footer class="main-footer">
                    <div class="pull-right hidden-xs">
                        <b>Version</b> 2.3.6
                    </div>
                    <strong>Copyright &copy; 2017-2018 <a href="#">Civil Service Commission (CSCUFO)</a>.</strong> All rights reserved.
                </footer>
                <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
        $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.6 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- iCheck 1.0.1 -->
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="assets/dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="assets/dist/js/demo.js"></script>
    </body>

    </html>
    <?php
  }else{
    header("location: index.php");
  }
?>