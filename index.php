<?php
@session_start();
include "config/dbconnect.php";

if(@$_SESSION['admin']){
  header("location: dashboard.php");
}else{
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login / LGU Urdaneta City</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
    <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <!-- Custom Fonts -->
  <link rel="stylesheet" type="text/css" href="assets/asset/font-awesome-4.6.3/css/font-awesome.min.css">

  <!-- Ionicons -->
  <link rel="stylesheet" type="text/css" href="assets/ionicons-2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

  <link rel="stylesheet" type="text/css" href="jerome.css">

 <!--For validation -->
    <!-- <link rel="stylesheet" href="assets/validation2/vendor/bootstrap/css/bootstrap.css"/> -->
   <link rel="stylesheet" href="assets/validation2/dist/css/bootstrapValidator.css"/>

   <script type="text/javascript" src="assets/validation2/vendor/jquery/jquery-1.10.2.min.js"></script>
   <script type="text/javascript" src="assets/validation2/vendor/bootstrap/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="assets/validation2/dist/js/bootstrapValidator.js"></script>

</head>
<body class="hold-transition login-page" style="background-image: url(images/files.jpg); background-repeat: no-repeat; background-size: cover;">
<div class="login-box">
  <div class="login-logo">
    <b style="color: white;">CSCUFO</b>
    <h5 style="color: white;">Civil Service Commission Urdaneta Field office</h5>
    
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    

    <form method="post">
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="User name">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <input type="submit" name="login" class="btn btn-primary btn-block btn-flat" value="Sign In">
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="#">I forgot my password</a><br>
    <a href="sign-up.php" class="text-center">Register a new membership</a>

    


    <?php
      $username = @$_POST['username'];
      $password = @$_POST['password'];
      $login = @$_POST['login'];

      if ($login) {
        if ($username == "" || $password == "") {
          ?><script type="text/javascript">alert("Username and Password is incorrect!")</script><?php
        }else{
        $login_query = mysqli_query($con,"SELECT * FROM tbl_user WHERE username = '$username' and password = md5('$password') ")or die(mysqli_error());
      $row = mysqli_num_rows($login_query);
      $result = mysqli_fetch_array($login_query);


          if ($row >= 1) {

            if ($result['user_type'] == "admin") {
                  @$_SESSION['admin'] = $result['id'];
                    //display session into applicant page
                    $_SESSION['id'] = $result['id'];
                    $_SESSION['user_type'] = $result['user_type'];


            if ($row > 0){
                  session_start();

                  header("location: dashboard.php");

              }

            }else{

            ?>
            <script type="text/javascript">
              alert("Invalid Username or Password! Please try again.");
              window.location.href='index.php'
            </script>
            <?php

          }
        }else{

          ?>
          <script>
               $(window).load(function(){
                   $('#loginFailed').modal('show');
                   setTimeout(function (){
                    $('#loginFailed').hide().modal('show');
                  });
               });
          </script>
          <?php
        }
        }
      }
    ?>

  </div>
  <!-- /.login-box-body -->


  <div id="invalidDOB" class="modal fade" role="dialog" aria-hidden="true" style="overflow-y:visible;">
    <div class="modal-dialog modal-m">
      <div class="modal-content">
        <div class="modal-header">
        <h3>
        <i class="fa fa-exclamation-circle text-red"></i> Invalid Birthdate!
        </h3>
        </div>
        <div class="modal-body">
          <p>Please enter a valid birthdate and try again&hellip;</p>
        </div>
        <div class="modal-footer">
            <a href="index.php" class="btn btn-default">OK</a>
          </div>
      </div>
    </div>
</div>


<div id="msgFailed" class="modal fade" role="dialog" aria-hidden="true" style="overflow-y:visible;">
    <div class="modal-dialog modal-m">
      <div class="modal-content">
        <div class="modal-header">
          <h3><i class="fa fa-exclamation-circle text-red"></i> Invalid Account!</h3>
        </div>
        <div class="modal-body">
          <p>Username and Password has been taken. Please create another account&hellip;</p>
        </div>
        <div class="modal-footer">
            <a href="index.php" class="btn btn-default">OK</a>
          </div>
      </div>
    </div>
</div>

<div id="msgSuccess" class="modal fade" role="dialog" aria-hidden="true" style="overflow-y:visible;">
    <div class="modal-dialog modal-success">
      <div class="modal-content">
        <div class="modal-header"><h3>Thank you for joining us!</h3></div>
        <div class="modal-body">
          <p>Just login your account to start your session then you can now go to the dashboard and get access to all the contents of our websites. We are looking forward to providing you with valuable content&hellip;</p>
        </div>
        <div class="modal-footer">
            <a href="index.php" class="btn btn-default">OK</a>
          </div>
      </div>
    </div>
</div>

    <!-- Modal -->
<div id="loginFailed" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
          <i class="fa fa-exclamation-circle text-red"></i> 
          Login Failed!
        </h4>
      </div>

        <div class="modal-body">
          <p>Check your username and password then try again.</p>
        </div>

      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

<?php } ?>